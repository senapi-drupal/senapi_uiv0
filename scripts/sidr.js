(function ($, _, Drupal, drupalSettings) {

  Drupal.behaviors.sidr_trigger = {
    attach: function (context, drupalSettings) {
      $(context)
          .find('.js-sidr-trigger')
          .once('sidr-trigger')
          .each(function () {
            var $trigger = $(this);

            //var options = $trigger.attr('data-sidr-options') || '{}';
            var options = '{"source":"#superfish-menu-accordion","side":"right","method":"toggle","renaming":false,"displace":false,"nocopy":false}';
            options = $.parseJSON(options);

            var $target = $(options.source);
            if ($target.length === 0) {
              Drupal.throwError('Target element not found: ' + options.source);
              return;
            }

            options.onOpenEnd = function () {
              var sidr = this;


            };

            options.onCloseEnd = function () {
              var sidr = this;

            };

            var htmlContent = '', idMenu = '#sidr-hidden';
            $.each([options.source], function (index, element) {
              var accordion = $(element).clone();
              accordion.attr('id', 'superfish-main-accordion-hidden');
              accordion.attr('class', 'sf-menu sf-main sf-style-none sf-accordion sf-expanded');
              accordion.removeClass('sf-horizontal sf-vertical sf-navbar sf-shadow sf-js-enabled');

              accordion.find('li').each(function () {
                $(this).removeAttr('style').removeClass('sfHover').attr('id', $(this).attr('id') + '-accordion');
              });

              //accordion.children('ul').removeAttr('style').not('.sf-hidden').addClass('sf-hidden');
              var parent = accordion.find('li.menuparent');
              for (var i = 0; i < parent.length; i++) {
                parent.eq(i).children('a').append('<span class="sf-sub-indicator"> »</span>');

                parent.eq(i).children('ul').removeClass('sf-hidden').slideDown('fast')
                    .end().addClass('sf-expanded');
              }

              var htmlBtnClose =  '<span id="superfish-main-accordion-hidden-close" class="fa fa-times-circle"></span>';

              htmlContent += '<div id="sidr-hidden">' +htmlBtnClose+ $("<div />").append(accordion).html() + '</div>';
            });

            $('body').append(htmlContent);
            var menuRef = $(idMenu);
            options.source = idMenu;

            $trigger.sidr(options);

            menuRef.remove();

            var accordionElement = $('#superfish-main-accordion-hidden'),
                button = accordionElement.find('a.menuparent,span.nolink.menuparent');
            accordionElement.addClass('sf-expanded').hide().removeClass('sf-hidden').show();

            button.on('click', function (e) {
              if ($(this).closest('li').children('ul').length > 0) {
                e.preventDefault();

                var parent = $(this).closest('li');

                /**if (parent.children('a.menuparent,span.nolink.menuparent').length > 0 && parent.children('ul').children('li.sf-clone-parent').length == 0) {
                  var cloneLink = parent.children('a.menuparent,span.nolink.menuparent').clone();
                  cloneLink.removeClass('menuparent sf-depth-1').children('.sf-sub-indicator').remove();
                  cloneLink = $('<li class="sf-clone-parent" />').html(cloneLink);
                  parent.children('ul').addClass('sf-has-clone-parent').prepend(cloneLink);
                }*/

                if (parent.hasClass('sf-expanded')) {
                  parent.children('ul').slideUp('fast', function () {
                    $(this).closest('li').removeClass('sf-expanded').end().addClass('sf-hidden').show();
                  });

                }
                else {
                  parent.children('ul').hide().removeClass('sf-hidden').slideDown('fast')
                      .end().addClass('sf-expanded')/*.children('a.sf-accordion-button')
                      .end().siblings('li.sf-expanded').children('ul')
                      .slideUp('fast', function () {
                        $(this).closest('li').removeClass('sf-expanded').end().addClass('sf-hidden').show();
                      })*/;
                }
              }
            });

            var sidrId = $trigger.data('sidr');
            var $sidr = $('#' + sidrId);

            var btnClose = $('#superfish-main-accordion-hidden-close');
            btnClose.on('click', function (e) {
              e.preventDefault();
              //alert("clickme");
              $.sidr('close', jQuery.sidr('status').opened);
            });

            $trigger
                .attr('aria-controls', sidrId)
                .attr('aria-expanded', false);

            if (options.nocopy && $target.length > 0) {
              var $inner = $('<div class="sidr-inner"></div>').append($target);
              $sidr.html($inner);
            }

            $trigger.click(function () {
              $(document.body).data('sidr.lastTrigger', this);
            });
          });

      $(document.body).once('sidr-unfocus')
          .bind('click keyup', function (e) {
            var openSidr = jQuery.sidr('status').opened;
            if (!openSidr) {
              return;
            }

            var isBlur = true;

            if ($(e.target).closest('.sidr').length !== 0) {
              isBlur = false;
            }

            if ($(e.target).closest('.js-sidr-trigger').length !== 0) {
              isBlur = false;
            }

            if (e.type === 'keyup' && e.keyCode == 27) {
              isBlur = true;
            }

            if (isBlur) {
              $.sidr('close', openSidr);

              if (e.type === 'keyup') {

                var lastTrigger = $(document.body).data('sidr.lastTrigger');
                if (lastTrigger) {
                  $(lastTrigger).focus();
                }
              }
            }
          });
    }
  };

})(window.jQuery, window._, window.Drupal, window.drupalSettings);