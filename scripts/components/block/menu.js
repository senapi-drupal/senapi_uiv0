(function ($, Drupal) {

    /*menu translation*/
    function toggleDropdown(e) {
        let _d, _m;
        _d = $(e.target).parent();
        _m = $('.dropdown-menu', _d);
        setTimeout(function () {
            const shouldOpen = e.type !== 'click' && _d.is(':hover');
            _m.toggleClass('show', shouldOpen);
            _d.toggleClass('show', shouldOpen);
            $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
        }, e.type === 'mouseleave' ? 800 : 0);
    }

    $('.layout-header')
        .on('mouseenter mouseleave', '.block-language .dropdown-toggle', toggleDropdown)
        .on('click', '.block-language .dropdown-menu a', toggleDropdown);



    function handlerScroll() {
        let maxStick = 70;
        let container = $('#wrapper');
        let header =$('.navbar');

        if (window.scrollY > maxStick) {
            container.addClass('container-stick');
            header.addClass('stick');
        } else {
            container.removeClass('container-stick');
            header.removeClass('stick');
        }
    }

    window.addEventListener('scroll', handlerScroll);

})(jQuery, Drupal);