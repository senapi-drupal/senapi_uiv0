'use strict';

const merge = require('webpack-merge');
const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

// Shared configuration.
const commonConfig = {
  context: path.resolve(__dirname, './'),
  entry: {
    main: path.resolve(__dirname, './scripts/main.js'),
  },
  output: {
    path: path.resolve(__dirname, 'js'),
    filename: '[name].js',
  },
  externals: {
    jquery: 'jQuery',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: {
        loader: 'babel-loader'
      },
    }],
  },
  plugins: [],
};

// Development configuration.
const developmentConfig = {
  //devtool: 'cheap-eval-source-map',
  //mode: 'production',
  plugins: [
  ],
};

// Production configuration.
const productionConfig = {
  devtool: 'source-map',
  plugins: [
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true,
      }),
    ]
  }
};

// Export config based on the current environment.
if (process.env.NODE_ENV === 'production') {
  module.exports = merge(commonConfig, productionConfig);
} else {
  module.exports = merge(commonConfig, developmentConfig);
}
