/**
 * @file
 * Fullcalendar View plugin JavaScript file.
 */

(function ($, Drupal) {
    Drupal.behaviors.fullcalendarView = {
        attach: function (context, settings) {
            $("body", context)
                .once("absCustomBehavior")
                .each(function () {
                    // Date entry clicked.
                    let slotDate;

                    // Day entry click call back function.
                    function dayClickCallback(date) {
                        slotDate = date;
                    }

                    $("#calendar").fullCalendar({
                        header: {
                            left: "prev,next today",
                            center: "title",
                            right: drupalSettings.rightButtons
                        },
                        defaultDate: drupalSettings.defaultDate,
                        locale: drupalSettings.defaultLang,
                        // Can click day/week names to navigate views.
                        navLinks: drupalSettings.navLinks !== 0,
                        editable: true,
                        eventLimit: true, // Allow "more" link when too many events.
                        events: drupalSettings.fullCalendarView,
                        eventOverlap: drupalSettings.alloweventOverlap !== 0,
                        dayClick: dayClickCallback,
                        eventRender: function (event, $el) {
                            // Event title with HTML markup.
                            $el.find("span.fc-title").html($el.find("span.fc-title").text());
                            // Popup tooltip.
                            if (event.description) {
                                if ($el.fullCalendarTooltip !== "undefined") {
                                    $el.fullCalendarTooltip(event.title, event.description);
                                }
                            }
                            // Recurring event.
                            if (event.ranges) {
                                return (
                                    event.ranges.filter(function (range) {
                                        if (event.dom) {
                                            let isTheDay = false;
                                            const dom = event.dom;
                                            for (let i = 0; i < dom.length; i++) {
                                                if (dom[i] === event.start.format("D")) {
                                                    isTheDay = true;
                                                    break;
                                                }
                                            }
                                            if (!isTheDay) {
                                                return false;
                                            }
                                        }
                                        // Test event against all the ranges.
                                        if (range.end) {
                                            return (
                                                event.start.isBefore(
                                                    moment.utc(range.end, "YYYY-MM-DD")
                                                ) &&
                                                event.end.isAfter(moment.utc(range.start, "YYYY-MM-DD"))
                                            );
                                        }
                                        return event.start.isAfter(
                                            moment.utc(range.start, "YYYY-MM-DD")
                                        );
                                    }).length > 0
                                ); // If it isn't in one of the ranges, don't render it (by returning false)
                            }
                        },
                        eventResize: function (event, delta, revertFunc) {
                            // The end day of an event is exclusive.
                            // For example, the end of 2018-09-03
                            // will appear to 2018-09-02 in the callendar.
                            // So we need one day subtract
                            // to ensure the day stored in Drupal
                            // is the same as when it appears in
                            // the calendar.
                            if (event.end && event.end.format("HH:mm:ss") === "00:00:00") {
                                event.end.subtract(1, "days");
                            }
                            // Event title.
                            const title = $($.parseHTML(event.title)).text();
                            if (
                                drupalSettings.updateConfirm === 1 &&
                                !confirm(
                                    title +
                                    " end is now " +
                                    event.end.format() +
                                    ". Do you want to save the change?"
                                )
                            ) {
                                revertFunc();
                            } else {
                                /**
                                 * Perform ajax call for event update in database.
                                 */
                                jQuery
                                    .post(
                                        drupalSettings.path.baseUrl +
                                        "fullcalendar-view-event-update",
                                        {
                                            eid: event.id,
                                            entity_type: drupalSettings.entityType,
                                            start: event.start.format(),
                                            end: event.end ? event.end.format() : "",
                                            start_field: drupalSettings.startField,
                                            end_field: drupalSettings.endField,
                                            token: drupalSettings.token
                                        }
                                    )
                                    .done(function (data) {
                                        // alert("Response: " + data);
                                    });
                            }
                        },
                        eventDrop: function (event, delta, revertFunc) {
                            // Event title.
                            const title = $($.parseHTML(event.title)).text();
                            const msg =
                                title +
                                " was updated to " +
                                event.start.format() +
                                ". Are you sure about this change?";
                            // The end day of an event is exclusive.
                            // For example, the end of 2018-09-03
                            // will appear to 2018-09-02 in the callendar.
                            // So we need one day subtract
                            // to ensure the day stored in Drupal
                            // is the same as when it appears in
                            // the calendar.
                            if (event.end && event.end.format("HH:mm:ss") === "00:00:00") {
                                event.end.subtract(1, "days");
                            }
                            if (drupalSettings.updateConfirm === 1 && !confirm(msg)) {
                                revertFunc();
                            } else {
                                /**
                                 * Perform ajax call for event update in database.
                                 */
                                jQuery
                                    .post(
                                        drupalSettings.path.baseUrl +
                                        "fullcalendar-view-event-update",
                                        {
                                            eid: event.id,
                                            entity_type: drupalSettings.entityType,
                                            start: event.start.format(),
                                            end: event.end ? event.end.format() : "",
                                            start_field: drupalSettings.startField,
                                            end_field: drupalSettings.endField,
                                            token: drupalSettings.token
                                        }
                                    )
                                    .done(function (data) {
                                        // alert("Response: " + data);
                                    });
                            }
                        },
                        eventClick: function (calEvent, jsEvent, view) {
                            jsEvent.stopPropagation();

                            slotDate = null;
                            if (drupalSettings.linkToEntity) {
                                // Open a new window to show the details of the event.
                                if (calEvent.url) {
                                    $('#event-modal-lg').modal('show', {data: calEvent});
                                    //window.open(calEvent.url);
                                    return false;
                                }
                            }

                            return false;
                        }
                    });

                    if (drupalSettings.languageSelector) {
                        // Build the locale selector's options.
                        $.each($.fullCalendar.locales, function (localeCode) {
                            $("#locale-selector").append(
                                $("<option/>")
                                    .attr("value", localeCode)
                                    .prop("selected", localeCode === drupalSettings.defaultLang)
                                    .text(localeCode)
                            );
                        });
                        // When the selected option changes, dynamically change the calendar option.
                        $("#locale-selector").on("change", function () {
                            if (this.value) {
                                $("#calendar").fullCalendar("option", "locale", this.value);
                            }
                        });
                    } else {
                        $(".locale-selector").hide();
                    }

                    $("#calendar").dblclick(function () {
                        if (
                            slotDate &&
                            drupalSettings.eventBundleType &&
                            drupalSettings.dblClickToCreate &&
                            drupalSettings.addForm !== ""
                        ) {
                            const date = slotDate.format();
                            // Open a new window to create a new event (content).
                            window.open(
                                drupalSettings.path.baseUrl +
                                drupalSettings.addForm +
                                "?start=" +
                                date +
                                "&start_field=" +
                                drupalSettings.startField,
                                "_blank"
                            );
                        }
                    });

                    $('#event-modal-lg').on('show.bs.modal', function (event) {
                        var data = event.relatedTarget.data;
                        var modal = $(this);
                        var dialog = modal.find('.modal-dialog');
                        var content = modal.find('.modal-content');

                        var elementBtn = {
                            close: `<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>`,
                            edit: (data.editForm) ? `<button id="edit" type="button" class="btn btn-secondary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>` : ``,
                        };

                        dialog.append(`<div id="control-icon" class="position-absolute" style="top:0; right: 0; pointer-events: visible;">
                                <div class="btn-group-vertical">
                                  ${elementBtn.close}
                                  ${elementBtn.edit}
                                </div>
                              </div>`);

                        if (data.editForm) {
                            modal.find('#edit').on('click', function (e) {
                                e.stopPropagation();
                                var a = document.createElement('a');
                                a.target = "_blank";
                                a.href = data.editForm;
                                a.click();
                            });
                        }

                        content.html('');

                        var element = {
                            title: (data.title) ? `<div class="item-header text-center"><h5>${data.title}</h5></div>` : ``,
                            content: (data.content) ? `<div class="item-summary">${data.content}</div>` : ``,
                            date: (data.dateStart === data.dateEnd)? `Fecha: <span class="font-weight-bold">${data.dateStart}</span>`: `Inicia : <span class="font-weight-bold">${data.dateStart}</span> <br/> Finaliza: <span class="font-weight-bold">${data.dateEnd}</span>`,
                            link: (data.link) ? `<div class="item-link"><span class="font-weight-bold">Enlace: </span><a class="" href="${data.link.url}" target="_blank">${(data.link.title)? data.link.title : data.link.url}</a></div>` : ``,
                            image: (data.image)? `<div class="item-image"><img class="img-fluid" src="${data.image.url}" width="${data.image.width}" height="${data.image.height}" alt="${data.image.alt}"></div>`: ``
                        };

                        $(content).append(`
                                        <div class="card">
                                            <div class="card-body pt-3 pb-3 pl-3 pr-5">
                                                <div class="row">
                                                    <div class="col-md-12 text-justify">
                                                        ${element.title}
                                                        ${element.image}
                                                        ${element.content}
                                                    </div>
                                                    <div class="col-md-12 text-center">${element.date}</div>
                                                    <div class="col-md-12 text-center">${element.link}</div>
                                                </div>    
                                            </div>
                                        </div>
                                        `);
                    });
                });

                $('#event-modal-lg').on('hide.bs.modal', function (event) {
                    $(this).find('.modal-dialog').children().not(':first').remove();
                });
        }
    };
})(jQuery, Drupal);
